
let createSkill = (~name, ~description, ~completed = false, ~treeLevel = 0, ~id) => ({
  id: id,
  name: name,
  description: description,
  completed: completed,
  treeLevel: treeLevel
} : Talent.skill)

let _reactSkills = [
  createSkill(~id = 1, ~name = "Components", ~description = "Learning how functional components work", ~completed = false, ~treeLevel = 0),
  createSkill(~id = 2, ~name = "SetState", ~description = "How to manage state", ~completed = false, ~treeLevel = 0),
  createSkill(~id = 3, ~name = "Props", ~description = "Applying data management", ~completed = false, ~treeLevel = 0)
];

let _reactLevel1Skills = [
  createSkill(~id = 4, ~name = "Lifecycle methods", ~description = "I can apply lifecycle methods to write idiomatic react", ~completed = false, ~treeLevel = 1),
  createSkill(~id = 5, ~name = "State callback functions", ~description = "I can pass state callback functions so I can manage parent state in child component", ~completed = false, ~treeLevel = 1)
]

let _reactLevel2Skills = [
  createSkill(~id = 6, ~name = "Higher Order Components", ~description = "I can create Higher Order Components to allow reusability between components", ~completed = false, ~treeLevel = 2),
  createSkill(~id = 7, ~name = "Render Props", ~description = "I can use render props to dynamically link components", ~completed = false, ~treeLevel = 2)
]

let _reasonRootSkills = [
  createSkill(~id = 1, ~name = "Typesystem", ~description = "Learn about the wonderous typesystem of ocaml baked in reason!", ~completed = false, ~treeLevel = 0),
  createSkill(~id = 2, ~name = "Destructuring", ~description = "Learn how to destructure in Reason",  ~completed = false, ~treeLevel = 0)
];

let _reasonLevel1Skills = [
  createSkill(~id = 3, ~name = "Recursive functions", ~description = "Learn how to use recursive functions efficiently",  ~completed = false, ~treeLevel = 1),
  createSkill(~id = 4, ~name = "Stub", ~description = "This is a stub",  ~completed = false, ~treeLevel = 1),
  createSkill(~id = 5, ~name = "Stub", ~description = "This is a stub",  ~completed = false, ~treeLevel = 1)
]

let reactTree : Talent.talent = ("React Basics", Logos.reactLogo, [_reactSkills, _reactLevel1Skills, _reactLevel2Skills])
let reasonTree : Talent.talent = ("ReasonML Basics", Logos.reasonLogo, [_reasonRootSkills, _reasonLevel1Skills])

type action =
  | Cycle
  | CompleteSkill(Talent.skill)

type state = list(Talent.talent)

let activeTree = state => {
  state |> List.hd
}

let cycleList = lst => {
  let currentTree = activeTree(lst)
  let rest = lst |> List.tl
  List.append(rest, [currentTree])
};

/* needs to support the 2d version now */
let updateSkill = (skills : list(list(Talent.skill)), skill : Talent.skill) =>
  skills
  |> List.mapi((index, skillSet) => {
    if (index !== skill.treeLevel) {
      skillSet
    } else {
      skillSet
        |> List.map((existingSkill : Talent.skill) => {
          {
            ...existingSkill,
            completed: skill.id === existingSkill.id || existingSkill.completed
          }
        });
    }
  });

let updateTalentSkills = (state : state, talent : Talent.talent) =>
  state
  |> List.tl
  |> List.rev_append([talent])

let component = ReasonReact.reducerComponent("TalentPage");

let make = (_children) => {
  ...component,
  initialState: () => [reactTree, reasonTree],
  reducer: (action, state : state) => {
    switch (action) {
    | Cycle => ReasonReact.Update(cycleList(state))
    | CompleteSkill(skill)  => {
      let tree = activeTree(state);
      let (name, logo, skills) = tree;

      let updatedSkills = updateSkill(skills, skill)

      ReasonReact.Update(updateTalentSkills(state, (name, logo, updatedSkills)));
    }
    }
  },
  render: self =>
    <div className="TalentPage">
      <button onClick={(_e) => self.send(Cycle)}>
        (ReasonReact.string("cycle"))
      </button>
      <TalentTree
        onSkillClick=(skill => self.send(CompleteSkill(skill)))
        tree=(activeTree(self.state))
      />
    </div>,
};
