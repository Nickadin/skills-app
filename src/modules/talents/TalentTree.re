/* Produces eg: Completed: 2 / 3 */
let completionText = (tree : Talent.talent) =>
  [
    "Completed:",
    tree |> Talent.skillsOf |> Talent.completedTrainingCount |> string_of_int,
    "/",
    tree |> Talent.skillsOf |> Talent.totalLength |> string_of_int
  ]
  |> List.fold_left((memo, str) => memo ++ " " ++ str, "")

let component = ReasonReact.statelessComponent("TalentTree");

let make = (~tree, ~onSkillClick, _children) => {
  ...component,
  render: _self =>
    <div className="TalentTree">
      <h1>(tree |> Talent.treeName |> ReasonReact.string)</h1>
        <div>
          ...(
            Array.mapi((index, skillSet) => {
              <TalentRow
                skills=(skillSet |> Array.of_list)
                logo=(tree |> Talent.logoOf)
                onSkillClick=(onSkillClick)
                isClickable=(index == 0 || Talent.hasCompletedTalentRow(
                  tree |> Talent.skillsOf,
                  index - 1
                ))
              />
            }, tree |> Talent.skillsOf |> Array.of_list)
          )
      </div>
      <span>
        (tree |> completionText |> ReasonReact.string)
      </span>
    </div>,
};
