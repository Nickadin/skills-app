let component = ReasonReact.statelessComponent("Skill");

let classNames = (skill: Talent.skill) => {
  switch (skill) {
  | { completed: true } => "Skill Skill__completed"
  | _ => "Skill"
  }
}

let nameOf = (skill : Talent.skill) => {
  skill.name
}

let descriptionOf = (skill : Talent.skill) => {
  skill.description
}

let make = (~skill : Talent.skill, ~logo : string, ~onClick, _children) => {
  ...component,
  render: _self => {
    <button onClick=(_e => onClick(skill)) className=(classNames(skill))>
      <div className="Skill__logo">
        <img src=(logo) />
        <span className="Skill__tooltip">
          <h2>(skill |> nameOf |> ReasonReact.string)</h2>
          (skill |> descriptionOf |> ReasonReact.string)
        </span>
      </div>
    </button>
  }
}