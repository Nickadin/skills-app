type skill = {
  id: int,
  name: string,
  description: string,
  completed: bool,
  treeLevel: int
};

type skillSet = list(list(skill)); /* actual skills of this talent, sorted in a 2d array to represent depth */

type talent = (
  string, /* name */
  string, /* logo */
  skillSet
);

let completedTrainingCount = skills =>
  skills
    |> List.map(skillSet => {
      skillSet
        |> List.filter(skill => switch (skill) {
        | { completed: true } => true
        | _ => false
        })
        |> List.length
    })
    |> List.fold_left((acc, num) => acc + num, 0);

let totalLength = skills =>
  skills
  |> List.map(List.length)
  |> List.fold_left((acc, num) => acc + num, 0);

let treeName = ((name, _, _)) => name;

let logoOf = ((_, logo, _)) => logo;

let skillsOf = ((_, _, skills)) => skills;

let hasCompletedTalentRow = (skillSet : skillSet, treeLevel : int) => {
  let incompleteSkills =
    skillSet
      ->  List.nth(treeLevel)
      |> List.filter(skill => !skill.completed)
      |> List.length;
  incompleteSkills == 0;
};