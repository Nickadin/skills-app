let component = ReasonReact.statelessComponent("TalentRow");

let classNames = (isClickable : bool) =>
  isClickable ? "TalentRow" : "TalentRow TalentRow__disabled";

let noop = (_skill : Talent.skill) => ();

let make = (~onSkillClick, ~skills : array(Talent.skill), ~logo : string, ~isClickable : bool, _children) => {
  ...component,
  render: _self =>
    <div className=(classNames(isClickable))>
      ...(
        Array.map(
          skill => {
            <Skill
              onClick=(isClickable ? onSkillClick : noop)
              skill=(skill)
              logo=(logo)
            />
          },
          skills
        )
      )
    </div>
}